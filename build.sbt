name := """twitter"""

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayScala)

scalaVersion := "2.11.7"

libraryDependencies ++= Seq(
  jdbc,
  cache,
  ws,
  filters,
  "org.scalatestplus.play" %% "scalatestplus-play" % "1.5.0-RC1" % Test,
  "com.github.t3hnar" %% "scala-bcrypt" % "2.6",
  "com.livestream" %% "scredis" % "2.0.6",
  "org.ocpsoft.prettytime" % "prettytime" % "3.2.7.Final"
)

// https://mvnrepository.com/artifact/org.apache.activemq/activemq-broker
libraryDependencies += "org.apache.activemq" % "activemq-broker" % "5.14.2"

// https://mvnrepository.com/artifact/org.apache.activemq/activemq-stomp
libraryDependencies += "org.apache.activemq" % "activemq-stomp" % "5.14.2"
// https://mvnrepository.com/artifact/org.apache.activemq/activemq-http
libraryDependencies += "org.apache.activemq" % "activemq-http" % "5.14.2"

// https://mvnrepository.com/artifact/org.fusesource.stompjms/stompjms-client
libraryDependencies += "org.fusesource.stompjms" % "stompjms-client" % "1.5"





resolvers += "scalaz-bintray" at "http://dl.bintray.com/scalaz/releases"
resolvers += "Sonatype OSS Releases" at "https://oss.sonatype.org/content/repositories/releases/"
resolvers += "ActiveMQ Release" at "https://mvnrepository.com/artifact/org.apache.activemq/activemq-core"