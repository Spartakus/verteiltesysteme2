if (window.console) {
  console.log("Welcome to your Play application's JavaScript!");
}

$(document).ready(function(){
    // Adding Sidenavigation
    $(".button-collapse").sideNav();
    $(".dropdown-button").dropdown();


    // Auto completion for user search
    var autocompleteInputs = $('input.autocomplete');

    var searchers = autocompleteInputs.map(function(){
        var map = new Object();
        map['drawer'] = $(this).data("activates");
        map['selector'] = $(this);
        return map;
    });

    $(searchers).each(function(idx){
      var drawer = this.drawer
      this.selector.materialize_autocomplete({
          limit: 20,
          dropdown: {
              el: '#' + drawer
          },
          onSelect: function(item){
              var url = jsRoutes.controllers.TimelineController.otherTimeline( item.id ,0).absoluteURL();
              window.location.href = url;
          },
          getData: function (value, callback) {
              var url = jsRoutes.controllers.SearchController.jsonsearch(value);

              $.ajax(url).done(function(data){
                  console.log("Result" + data);
                  callback(value, data);
              });
          }
      });
    });

    $.debounce(200, false, function(){
        $('input:-webkit-autofill').each(function(){
            if ($(this).val().length !== "") {
                $(this).siblings('label, i').addClass('active');
            }
        });
    });



    // Now call Callbacks in Mgr
    CallbackMgr.callCallbacks();

});