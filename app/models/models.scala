//package models
/**
  * Created by domi on 12-Oct-16.
  */

package object models {

  type UserId = Long
  type MessageId = Long

  /* User Definitions */
  /* Having a:
     * Base User with an Email and a password
     * Login User with Email and Password (as Case class)
     * Basic User with Eamil username and passwort
     * User Like BasicUser with Following and Followers
  */

  sealed abstract class BaseUser {
    //def username: String
    def email: String
    def password: String
  }

  case class LoginUser( email: String, password: String ) extends BaseUser
  case class BasicUser( username: String, email: String, password: String ) extends BaseUser
  case class User( id: UserId, username: String, email: String, password: String, following: List[UserId], followers: List[UserId] ) extends BaseUser


  /*
  implicit class Map2Case(values: Map[String,String]){
    def convert[A](implicit mapper: MapConvert[A]):A = mapper conv values
  }

  trait MapConvert[A] {
    def conv(values: Map[String, String]): A
  }

  object User {
    implicit val converter =  new MapConvert[Option[User]]{
      def conv(values: Map[String, String]): Option[User] = try {
        Some( User(values("id").toLong, values("email"), values("username"), values("password"), List.empty, List.empty) )
      } catch {
        case ex: Exception => None
      }
    }
  }*/

  sealed abstract class BaseMessage {
    def body: String
  }

  case class postMessage( body: String ) extends BaseMessage
  case class Message(id: MessageId, owner: UserId, body: String, date: String ) extends BaseMessage
  case class PrintMessage(id: MessageId, owner: User, body: String, date: String ) extends BaseMessage

}



