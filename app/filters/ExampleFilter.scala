package filters

import akka.stream.Materializer
import javax.inject._

import play.api.{Environment, Logger}
import play.api.http.HeaderNames
import play.api.mvc._

import scala.concurrent.{ExecutionContext, Future}

/**
 * This is a simple filter that adds a header to all requests. It's
 * added to the application's list of filters by the
 * [[ExampleFilters]] class.
 *
 * @param mat This object is needed to handle streaming of requests
 * and responses.
 * @param exec This class is needed to execute code asynchronously.
 * It is used below by the `map` method.
 */
@Singleton
class ExampleFilter @Inject()(
    implicit override val mat: Materializer,
    exec: ExecutionContext) extends Filter {

  override def apply(nextFilter: RequestHeader => Future[Result])
           (requestHeader: RequestHeader): Future[Result] = {
    // Run the next filter in the chain. This will call other filters
    // and eventually call the action. Take the result and modify it
    // by adding a new header.
    val splitPattern = """^(.*:\/\/)?([A-Za-z0-9\-\.]+)(:[0-9]+)?(.*)$""".r
    val host = requestHeader.headers.get("Host").getOrElse("0.0.0.0")
    //Logger.debug("(1) Request on: " + host )

    val realHostMatches = splitPattern.findAllIn(host)
    realHostMatches.hasNext //init regex
    val realHost = realHostMatches.group(2)
    val websockhost = realHost + ":61616"

    //Logger.debug("(2) Request to: " + websockhost )
    nextFilter(requestHeader).map { result =>
      result.withHeaders(
        "X-ExampleFilter" -> "foo",
        HeaderNames.ACCESS_CONTROL_ALLOW_ORIGIN -> websockhost,
        HeaderNames.ALLOW -> "*",
        HeaderNames.ACCESS_CONTROL_ALLOW_METHODS -> "POST, GET, PUT, DELETE, OPTIONS",
        HeaderNames.ACCESS_CONTROL_ALLOW_HEADERS -> "Origin, X-Requested-With, Content-Type, Accept, Referer, User-Agent"
      )
    }
  }

}
