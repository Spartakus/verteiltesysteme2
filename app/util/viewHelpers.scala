package util

import java.util.Date

import controllers.routes
import models.{PrintMessage, User}
import org.ocpsoft.prettytime.PrettyTime
import play.api.mvc.Call;
/**
  * Created by domi on 08.12.2016.
  */
object viewHelpers {
  def displayTime(timestamp:String): String = {
    val date = new Date()
    date.setTime(timestamp.toLong)
    val pt = new PrettyTime()
    val out = pt.format(date)
    out
  }

  def displayTwoot(twoot: PrintMessage) = {
    val path= routes.Assets.versioned("images/profile_pictures/0.jpg")

    val test = <li class="collection-item avatar">
        <img src={path.toString} alt="" class="circle responsive-img"/>
        <a href={"/users/" + twoot.owner.id}><span class="title">{twoot.owner.username}</span></a>
        <p>
          {twoot.body}
        </p>
        <span class="date">{displayTime(twoot.date)}</span>
        <a href="#!" class="secondary-content"><i class="material-icons">grade</i></a>
    </li>
    test
  }

  def foreignHeader(timelineAuth: User, authUser: Option[User]) = {
    val path= routes.Assets.versioned("images/profile_pictures/0.jpg")

    val infoBox = <div class="row">
                    <div class="col s12">
                      <div class="flexChild columnParent">

                        <div id="columnChild" class="flexChild rowParent">
                          <div id="" class="flexChild rowChild" style="text-align: right">
                            <img src={path.toString} alt="" class="smallavater"/>
                          </div>


                          <div id="" class="flexChild rowChild">
                            <a href={"/users/"+timelineAuth.id}>{timelineAuth.username}</a>
                          </div>

                          <div id="" class="doubleFlexChild rowChild columnParent">


                              <div id="" class="doubleFlexChild">
                                Follows {timelineAuth.following.size}
                              </div>
                              <div id="" class="doubleFlexChild">
                                Follower {timelineAuth.followers.size}
                              </div>

                          </div>


                          {authUser.map { user =>
                            if (user.id != timelineAuth.id){
                              <div class="flexChild rowChild columnParent">
                                <div id="" class="flexChild" style="text-align: right">
                                    {if(timelineAuth.followers.contains(user.id) ) {
                                      <a href={routes.TimelineController.unfollow(timelineAuth.id).toString}>Entfolgen</a>
                                    }else{
                                      <a href={routes.TimelineController.follow(timelineAuth.id).toString}>Folgen</a>
                                    }}
                                </div>
                              </div>
                            }
                          }.getOrElse("")}

                        </div>
                      </div>
                    </div>
                  </div>

    val test = <div class="center-align margined">
      <img src={path.toString} alt="" class="smallavater left-align"/> |
        <a href={"/users/"+timelineAuth.id}>{timelineAuth.username}</a> |
        Folgt <strong>{timelineAuth.following.size}</strong> |
        Follower <strong>{timelineAuth.followers.size}</strong> |
        {authUser.map { user =>
          if (user.id != timelineAuth.id){
            if(timelineAuth.followers.contains(user.id) ) {
               <a href={routes.TimelineController.unfollow(timelineAuth.id).toString}>Entfolgen</a>
            }else{
              <a href={routes.TimelineController.follow(timelineAuth.id).toString}>Folgen</a>
            }
          }
        }.getOrElse("")}
    </div>
    infoBox
  }

  def pager(currentPage: Long, amountOfItems: Long, pageSize: Long, controllerAction: (Long) => Call ) = {

    val output =  <div class="row">
                    <div class="col s6 offset-s3 center-align">
                      {
                        if(amountOfItems > pageSize) {
                          if (currentPage > 0) {
                            <a href={controllerAction(currentPage - 1).url}>Seite zurück</a>
                          }
                        }
                      }
                      {
                        if (amountOfItems > pageSize) {
                          <span>Seite: {currentPage + 1}</span>
                        }
                      }
                      {
                        if(amountOfItems > pageSize) {
                          if (((currentPage + 1) * pageSize) < amountOfItems) {
                            <a href={controllerAction(currentPage + 1).url}>Seite vor</a>
                          }
                        }
                      }
                    </div>
                  </div>
    output
  }

}
