package wrappers

import com.google.inject.Inject
import models.User
import play.api.mvc._
import services.SessionService

import scala.concurrent.Future

/**
  * Created by domi on 22.11.2016.
  *//*
class UserRequest[A] @Inject()(sese: SessionService)(val user: Option[User], request: Request[A]) extends WrappedRequest[A](request)

object UserAction extends ActionBuilder[UserRequest] with
  ActionTransformer[Request, UserRequest]{
  /*
  def Authenticated(f: UserRequest => Result) = {
    val authstr = sese.getSession(request)
    authstr match {
      case Some(str) =>
        val islegit = sese.isLegitSession(str)
        val result = islegit.map { isValid =>
          if (isValid) action(request)
          else Future {
            Results.Forbidden("Nop")
          }
        }
        result.flatMap(identity)
      case None => Future {
        Results.Forbidden("Go to hell, boy")
      }
    }
  }*/

  val sese: SessionService
  def transform[A](request: Request[A]) = {
    val getUserOfSession = sese.getBySession(request)
    getUserOfSession.map {
      case Some(user) => new UserRequest(Some(user), request)
      case None       => new UserRequest(None, request)
    }
  }
}*/