package wrappers
import play.api.mvc.Results
import play.api.mvc._
import com.google.inject.{Inject, Singleton}
import play.api.mvc.{Action, ActionBuilder, Request, Result}
import services.{SessionService, UserService}
import play.api.libs.concurrent.Execution.Implicits.defaultContext

import scala.concurrent.Future

/**
  * Created by domi on 21.11.2016.
  */

/*
class AuthenticatedAction[A] @Inject()(sese: SessionService)(action: Action[A])   extends Action[A] {
  lazy val parser = action.parser
  def apply(request: Request[A]): Future[Result] = {
    val authstr = sese.getSession(request)
    authstr match {
      case Some(str) =>
        val islegit = sese.isLegitSession(str)
        val result = islegit.map { isValid =>
          if (isValid) action(request)
          else Future {
            Results.Forbidden("Nop")
          }
        }
        result.flatMap(identity)
      case None => Future {
        Results.Forbidden("Go to hell, boy")
      }
    }
    //action(request)
  }
}
/*
@Singleton
class AuthenticatedAction @Inject()(sese: SessionService) extends ActionBuilder[Request] {


    def invokeBlock[A](request: Request[A], block: (Request[A]) => Future[Result]): Future[Result] = {
      val authstr = sese.getSession(request)
      authstr match {
        case Some(str) =>
          val islegit = sese.isLegitSession(str)
          val result = islegit.map { isValid =>
            if (isValid) block(request)
            else Future {
              Results.Forbidden("Nop")
            }
          }
          result.flatMap(identity)
        case None => Future {
          Results.Forbidden("Go to hell, boy")
        }
      }
      /*val session = request.session.get("authstring")
      session match {
        case Some(sesskey) =>
          block(request)
        case None => Future {
          Results.Forbidden("Go to hell, boy")
        }
      }*/
  }
}*/*/