package wrappers

import com.google.inject.Inject
import models.User
import play.api.mvc.Security.AuthenticatedRequest
import play.api.mvc._
import services.{SessionService, SessionServices, UserService}
import scala.concurrent.ExecutionContext.Implicits.global

import scala.concurrent.Future

/**
  * Created by domi on 22.11.2016.
  */
class   AuthRequest[A](val user: User, request: Request[A]) extends WrappedRequest[A](request)


trait CustomActionBuilders{
  // the abstract dependency
  def sesser: SessionServices
  def uese: UserService

  def AuthenticatedAction = new ActionBuilder[AuthRequest] {
    def invokeBlock[A](request: Request[A], block: (AuthRequest[A]) => Future[Result]): Future[Result] = {
      val authstr = sesser.getBySession(request)
      authstr.map {
        case Some(user) => block( new AuthRequest[A](user, request) )
        case None => Future.successful(Results.Forbidden("nope"))
      }.flatMap(identity)
    }
  }
}