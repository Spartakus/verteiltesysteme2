package services

import javax.inject.Singleton

import com.google.inject.Inject
import dao.UserDAO
import models.{BasicUser, User, UserId}

import scala.concurrent.Future


/**
  * Created by domi on 12-Oct-16.
  */

@Singleton
class UserService @Inject() (userdao: UserDAO){
  def getUsers = userdao.listAll
  def getById( id: UserId ) = userdao.getById( id )
  def add( user: BasicUser ) = userdao.addUser(user)
  def findByMail(email: String) = userdao.findByMail( email )
  def findByMailAndPw(email: String, pw: String) = userdao.findByMailAndPw(email, pw)
  def login(email: String, pw: String) = userdao.login( email, pw )
  def getFollowing(user: User) = userdao.getFollowing(user)
  def getFollowers(user: User) = userdao.getFollowers(user)
  def addFollowing(target: User, following: User) = userdao.addFollowing(target, following)
  def addFollowers(target: User, follower: User) = userdao.addFollowers(target, follower)
  def removeFollowing(target: User, following: User) = userdao.removeFollowing(target, following)
  def removeFollowers(target: User, follower: User) = userdao.removeFollowers(target, follower)
  def delete(userId: UserId) =  userdao.delete(userId)

  def searchUsers(pattern: String) = userdao.searchUsersByPattern(pattern)

  def addAuthString(user: User) = userdao.addAuthString(user)
  def delAuthString(user: User) = userdao.delAuthString(user)
  def getUserByAuth(authString: String) = userdao.getUserByAuth(authString)
}
