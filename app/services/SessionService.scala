package services

import com.google.inject.{Inject, Singleton}
import models.User
import play.api.mvc.Request
import play.api.libs.concurrent.Execution.Implicits.defaultContext

import scala.concurrent.Future

/**
  * Created by domi on 21.11.2016.
  */
@Singleton
class SessionService @Inject() (us: UserService) extends SessionServices {
  override def addSession[A](implicit request: Request[A], use: User): Future[Option[(String, String)]] = {
    val authstr = us.addAuthString(use)
    authstr.map {
      case None => None
      case Some(str) =>
        Some(("userauth", str))
    }
  }

  override def delSession[A](implicit request: Request[A]) : Future[Boolean] = {
    val authStr = request.session.get("userauth")
    authStr match {
      case None => Future.successful(false)
      case Some(authstr) =>
        val user = us.getUserByAuth(authstr)
        val doRemove = user.map {
          case None => Future.successful(false)
          case Some(loggedIn) => us.delAuthString(loggedIn)
        }
        doRemove.flatMap(identity)
    }
  }

  override def getSession[A](implicit request: Request[A]) : Option[String] = {
    request.session.get("userauth")
  }

  override def isLegitSession(userauth: String): Future[Boolean] = us.getUserByAuth(userauth).map {
    case Some(usr) => true
    case None => false
  }

  override def getBySession[A](implicit request: Request[A]): Future[Option[User]] = {
    val authStr = request.session.get("userauth")
    val res1 = authStr match {
      case Some(key) =>
        us.getUserByAuth(key)
        //Future.successful(key)
      case None => Future.successful(None)
    }
    res1
  }

}
