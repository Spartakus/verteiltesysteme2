package services

import akka.actor.{Actor, ActorRef, Props}
import scredis.{PubSubMessage, Redis}

import scala.util.matching.Regex
/**
  * Created by domi on 24.12.2016.
  */


object WebSocketWorker {
  def props(out: ActorRef) = Props(new WebSocketWorker(None, out))
  def speficicprops(id: Option[Int], out: ActorRef) = Props( new WebSocketWorker(id, out) )
}
class WebSocketWorker(id: Option[Int], out: ActorRef) extends Actor {

  val scredis = Redis()

  id match {
    case Some(idNr) => createSubscriber(s"Timeline $idNr")
    case None => createSubscriber(s"GlobalTimeline")
  }

  def createSubscriber(chan: String) = scredis.subscriber.subscribe(chan) {
    case message @ PubSubMessage.Message(channel, messageBytes) => {
      val messageString = message.readAs[String]()
      out ! messageString
    }
    case PubSubMessage.Subscribe(channel, channelsCount) => // client subscribed to channel
    case PubSubMessage.Unsubscribe(channel, channelsCount) => // client unsubscribed from channel
    case PubSubMessage.Error(exception) => {
      // an error occurred, e.g. NOAUTH when trying to subscribe without being authenticated
    }
  }

  val ListenRegex = "Listen ([0-9]+)".r

  def receive = {
    case msg: String =>
      msg match {
        case ListenRegex(lid) => out ! ("I Listen to " + lid)
          createSubscriber(s"Timeline $lid")
        case _               => out ! ("I received your message: " + msg)
      }
  }

  override def postStop(): Unit = {
    scredis.subscriber.unsubscribe()
  }

}
