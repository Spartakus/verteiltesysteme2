package services

import javax.inject.Singleton

import com.google.inject.Inject
import dao.MessageDAO
import models._

import collection.{mutable => m}
import scala.concurrent.Future
/**
  * Created by domi on 12-Oct-16.
  */
@Singleton
class MessageService @Inject()( msgDao: MessageDAO ) {
  def getById( id: MessageId ) = msgDao.getById(id)
  def addMessage( msg: postMessage, user: User )= msgDao.addMessage(msg, user)
  def getTimelineOfUser( user: User, offset: Long = 0, limit: Option[Long]) = msgDao.getTimelineOfUser(user.id, offset, limit)
  def getGlobal( offset: Long = 0, limit: Option[Long]) = msgDao.getGlobal(offset, limit)
  def removeMsg(msg: Message) = msgDao.removeMessage(msg)

  def amountOnTimeline(user: User) = msgDao.getAmountOnTimeline(user.id)
  def amountByAuthor(user: User) = msgDao.getAmountByAuthor(user.id)
  def amountGlobal = msgDao.getAmountGlobal

  def removeMessagesOfAuthorFromTimeline( userWithTimeline: UserId, userWithMsgToBeDeleted: UserId  ) = msgDao.removeMessagesOfAuthorFromTimeline(userWithTimeline, userWithMsgToBeDeleted)
  def addMessagesOfAuthorToTimeline( userWithTimeline: UserId, userWithMsgToBeadded: UserId  ) = msgDao.addMessagesOfAuthorToTimeline(userWithTimeline, userWithMsgToBeadded)

  def convertMessage(msg: Message): Future[Option[PrintMessage]] = msgDao.convertMessage(msg)
}
