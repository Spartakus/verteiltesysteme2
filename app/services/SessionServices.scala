package services
import models.User
import play.api.mvc.Request

import scala.concurrent.Future

/**
  * Created by domi on 22.11.2016.
  */
trait SessionServices {

  def addSession[A](implicit request: Request[A], use: User): Future[Option[(String, String)]]
  def delSession[A](implicit request: Request[A]): Future[Boolean]
  def getSession[A](implicit request: Request[A]): Option[String]
  def isLegitSession(userauth: String): Future[Boolean]
  def getBySession[A](implicit request: Request[A]): Future[Option[User]]
}
