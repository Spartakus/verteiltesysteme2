package services

import com.google.inject.{Inject, Singleton}
import models.{Message, PrintMessage, User, UserId}
import play.api.Logger
import play.api.libs.json.{JsObject, Json, Writes}
import org.apache.activemq.transport.stomp.StompConnection
import play.api.inject.ApplicationLifecycle

import scala.concurrent.Future

/**
  * Created by domi on 09.01.2017.
  */
trait PublishToQueue {
  def publishMessageOnTimeline(timelineId: UserId, message: PrintMessage): Unit
  def publishMessageGlobally( message: PrintMessage ): Unit
}

@Singleton
class PublishToStomp @Inject()(lifecycle: ApplicationLifecycle) extends PublishToQueue {

  /* Queue Implementation */
  val connection = new StompConnection()
  connection.open("localhost", 41414)
  connection.connect("","")

  Logger.debug("starting publisher")

  /*
    case class User( id: UserId, username: String, email: String, password: String, following: List[UserId], followers: List[UserId] )
  */
  implicit val userWrites: Writes[User] = new Writes[User] {
    def writes(user: User): JsObject = Json.obj(
      "id" -> user.id,
      "username" -> user.username
    )
  }

  implicit val messageWrites: Writes[PrintMessage] = new Writes[PrintMessage] {
    def writes(msg: PrintMessage): JsObject = Json.obj(
      "date" -> msg.date,
      "body" -> msg.body,
      "id" -> msg.id,
      "owner" -> msg.owner
    )
  }

  def publishMessageOnTimeline(timelineId: UserId, message: PrintMessage): Unit = {
    connection.send(s"/topic/timeline/$timelineId", Json.stringify(Json.toJson(message)))
  }

  def publishMessageGlobally(message: PrintMessage): Unit = {
    connection.send(s"/topic/timeline/global", Json.stringify(Json.toJson(message)))
  }

  lifecycle.addStopHook { () =>
    Logger.debug("Stopping publisher")
    Future.successful( connection.disconnect() )
    Future.successful(Logger.debug("Stopped publisher"))
  }
}