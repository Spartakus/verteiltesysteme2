package services
import java.io.IOException
import javax.inject._

import org.apache.activemq.broker.BrokerService
import play.api.Logger
import play.api.inject.ApplicationLifecycle
import org.apache.activemq.util.IOExceptionHandler

import scala.concurrent.Future
import play.api.libs.concurrent.Execution.Implicits.defaultContext



/**
  * Created by domi on 05.01.2017.
  */
trait QueueLauncher {}

@Singleton
class QueueLauncherImpl @Inject()(lifecycle: ApplicationLifecycle) extends QueueLauncher {
  Logger.debug("Starting Queue")

  var broker = new BrokerService()

  broker.addConnector("ws://localhost:61616")
  broker.addConnector("stomp://localhost:41414")

  broker.setIoExceptionHandler {
    new IOExceptionHandler {
      override def handle(exception: IOException) = {
        Logger.error(exception.getMessage, exception.getCause)
        val stcktrc = exception.getStackTrace
        stcktrc.foreach{ stackElement =>
          Logger.error(stackElement.getFileName + " " + stackElement.getLineNumber +" "+ stackElement.getMethodName)
        }
      }

      override def setBrokerService(brokerService: BrokerService): Unit = {
        Logger.debug("I Got a Brokerservice! " + " " + brokerService.getNetworkConnectorURIs )
      }
    }
  }
  if ( !broker.isStarted ) {
    broker.setPersistent( false )
    val systemUsage = broker.getSystemUsage

    // Setting temp size. ActiveMQ uses 50 GB by default... lol
    systemUsage.getTempUsage.setLimit( 100 * 1024 * 1024 * 1024 ) // 1024 Byte * 1024 = 1 KByte | Kbyte * 1024 = 1 Mbyte | 1 Mbyte * 100 = 100 Mb
    systemUsage.getMemoryUsage.setLimit( 100 * 1024 * 1024 * 1024 )
    /*val topic = new ActiveMQTopic( "jms.topic.test" )
    val topicAsDest: ActiveMQDestination = topic
    val destinations = Array( topicAsDest )
    broker.setDestinations( destinations  )*/

    //val  managementContext = new ManagementContext()
   // managementContext.setCreateConnector( true )
    //broker.setManagementContext( managementContext )

    broker.start()

    if ( !broker.isStarted ) Logger.debug("Error Starting Broker")


    // Stop Broker on Play stop
    lifecycle.addStopHook { () =>
      Logger.debug("Stopping Queue")
      Future.successful( broker.stop() )
      Future.successful(Logger.debug("Stopped Queue"))
    }
  }
}