package services

import com.google.inject.{Inject, Singleton}
import models.{MessageId, UserId}
import scredis.{PubSubMessage, Redis}
import play.api.libs.concurrent.Execution.Implicits._
/**
  * Created by domi on 09.01.2017.
  */

trait RedisToQueue {}

@Singleton
class RedisToQueueImpl @Inject()(publish2Queue: PublishToQueue, messageService: MessageService) extends RedisToQueue {

  val scredis = Redis()

  val timelineRegex =       """Timeline ([0-9])+""".r

  private def publishToTimeLine(msgId: MessageId, timeline: UserId) = {
    val message = messageService.getById(msgId)
    message.map {
      case Some(msg) =>
        val printableMessage = messageService.convertMessage(msg)
        printableMessage.map {
          case Some(pMsg) =>
            publish2Queue.publishMessageOnTimeline(timeline, pMsg)
          case None =>
        }
      case None =>
    }
  }

  private def publishToGlobal(msgId: MessageId) = {
    val message = messageService.getById(msgId)
    message.map {
      case Some(msg) =>
        val printableMessage = messageService.convertMessage(msg)
        printableMessage.map {
          case Some(pMsg) =>
            publish2Queue.publishMessageGlobally(pMsg)
          case None =>
        }
      case None =>
    }
  }

  scredis.subscriber.pSubscribe("*") {
    case message @ PubSubMessage.PMessage(pattern, channel, messageBytes) => {
      val messageId: MessageId = message.readAs[String]().toLong
      channel match {
        case timelineRegex(number) => publishToTimeLine(messageId, number.toLong)
        case "GlobalTimeline"      => publishToGlobal(messageId)
        case _ =>
      }
    }
    case PubSubMessage.Subscribe(channel, channelsCount) => // client subscribed to channel
    case PubSubMessage.Unsubscribe(channel, channelsCount) => // client unsubscribed from channel
    case PubSubMessage.Error(exception) => {
      // an error occurred, e.g. NOAUTH when trying to subscribe without being authenticated
    }
  }
}
