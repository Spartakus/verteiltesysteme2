package controllers

import javax.print.attribute.standard.PrinterMessageFromOperator

import com.google.inject.{Inject, Singleton}
import models._
import play.api.data._
import play.api.data.Forms._
import play.api.i18n.{I18nSupport, MessagesApi}
import play.api.mvc.{Action, AnyContent, Controller, Results}
import services.{MessageService, SessionService, SessionServices, UserService}
import play.api.libs.concurrent.Execution.Implicits.defaultContext

import scala.concurrent.Future

/**
  * Created by domi on 02.11.2016.
  */
@Singleton
class TimelineController  @Inject()(sesser: SessionServices, uese: UserService, msgse: MessageService, val messagesApi: MessagesApi ) extends Controller with Secured with I18nSupport{

  val pageSize:Long = 10L

  val twootFormular :Form[models.postMessage] = Form(
    mapping(
      "body" -> nonEmptyText(minLength = 3, maxLength = 255)
    )(postMessage.apply)(postMessage.unapply)
  )

  private def amountOfTwootsByTimeLine(user: Option[User]): Future[Long] = {
    user match {
      case Some(usr) => msgse.amountOnTimeline(usr)
      case None => Future.successful(0)
    }
  }

  private def readTwootsByTimeLine(user: Option[User], offset: Long, max: Long): Future[List[PrintMessage]] = {
    user match {
      case Some(usr) =>
        val usertwoots = msgse.getTimelineOfUser(usr, offset, Some(max))
        val collection = usertwoots.map { list =>
          val listFutureMaybeMessages = for {
            entry <- list
          } yield for {
            user <- convertMessage(entry)
          } yield user
          val futureListMaybeMessages = Future.sequence(listFutureMaybeMessages)
          futureListMaybeMessages.map { list =>
            list.flatten
          }
        }
        collection.flatMap(identity)
      case None => Future.successful(Nil)
    }
  }


  def index(page: Long): Action[AnyContent]  = withUser { implicit ctx => implicit req =>
    Future.successful(Redirect(routes.TimelineController.otherTimeline(ctx.user.get.id, page)))
  }

  def follow(user: UserId): Action[AnyContent]  = withUser{ implicit  ctx => implicit  req =>
    ctx.user match {
      case Some(usr) =>
        val follow = userservice.getById(user)
        follow.map {
          case Some(us) =>
            val addFollow = userservice.addFollowers(usr, us)
            val addFollowing = userservice.addFollowing(us, usr)
            val addTimeline = msgse.addMessagesOfAuthorToTimeline(usr.id, us.id)
            val result = for {
              aFollow <- addFollow
              aFollowing <- addFollowing
              aTimeline <- addTimeline
            } yield (aFollow && aFollowing && aTimeline)

            result.map {
              case true => Redirect(routes.TimelineController.otherTimeline(usr.id, 0))
              case false => Redirect(routes.TimelineController.otherTimeline(usr.id, 0))
            }
          case None => Future.successful(Results.Forbidden)
        }.flatMap(identity)
      case None => Future.successful(Results.Forbidden)
    }
  }

  def unfollow(user: UserId): Action[AnyContent]  = withUser{ implicit  ctx => implicit  req =>
    ctx.user match {
      case Some(usr) =>
        val follow = userservice.getById(user)
        follow.map {
          case Some(us) =>
            val remFollow = userservice.removeFollowers(usr, us)
            val remFollowing = userservice.removeFollowing(us, usr)
            val remTimeline = msgse.removeMessagesOfAuthorFromTimeline(usr.id, us.id)
            val result = for {
              rFollow <- remFollow
              rFollowing <- remFollowing
              rTimeline <- remTimeline
            } yield (rFollow && rFollowing && rTimeline)

            result.map {
              case true => Redirect(routes.TimelineController.otherTimeline(usr.id, 0))
              case false => Redirect(routes.TimelineController.otherTimeline(usr.id, 0))
            }
          case None => Future.successful(Results.Forbidden)
        }.flatMap(identity)
      case None => Future.successful(Results.Forbidden)
    }
  }

  def twoot(): Action[AnyContent]  = withUser { implicit ctx => implicit req =>
    val offset = 0
    val max = this.pageSize - 1
    val result = twootFormular.bindFromRequest.fold(
      formWithErrors => {
        val usersTwoots = this.readTwootsByTimeLine(ctx.user, offset, max)
        val amount = this.amountOfTwootsByTimeLine(ctx.user)
        for {
          list <- usersTwoots
          amt <- amount
        } yield Ok(views.html.timeline( ctx.user.get, formWithErrors, list, amt, pageSize, 0))

      },
      message => {
        val newMessage = msgse.addMessage(message, ctx.user.get)
        newMessage.map {
          case true => Redirect(routes.TimelineController.otherTimeline(ctx.user.get.id, 0)).flashing("success" -> "Twoot erfolgreich!")
          case false => Redirect(routes.TimelineController.otherTimeline(ctx.user.get.id, 0)).flashing("errors" -> "Datenbank-Fehler!")
        }
      }
    )

    result
  }

  def otherTimeline(userid: UserId, page: Long): Action[AnyContent] = withUser { implicit ctx => implicit req =>
    val maybeUser = uese.getById(userid)

    val result = maybeUser.map { user =>

      val offset = page * this.pageSize
      val max = this.pageSize - 1
      val usersTwoots = this.readTwootsByTimeLine(user, offset, max)
      val amount = this.amountOfTwootsByTimeLine(user)
      user match {
        case Some(usr) =>
          for {
          list <- usersTwoots
          amt <- amount
          } yield Ok( views.html.timeline(usr, twootFormular, list, amt, pageSize, page))

        case None =>
          Future.successful(Results.NotFound)
      }
    }
    result.flatMap(identity)
  }

  def convertMessage(msg: Message): Future[Option[PrintMessage]] = {
      val user = uese.getById(msg.owner)
      user.map{
        case Some(usr) => Some(PrintMessage(msg.id, usr, msg.body, msg.date))
        case None => None
      }
  }
}
