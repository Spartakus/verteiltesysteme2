package controllers

import akka.actor.ActorSystem
import akka.stream.Materializer
import com.google.inject.Inject
import models.User
import play.api.libs.streams.ActorFlow
import play.api.mvc.{Controller, Results, WebSocket}
import services.WebSocketWorker

/**
  * Created by domi on 24.12.2016.
  */
class WebsocketController @Inject() (implicit system: ActorSystem, materializer: Materializer) extends Controller with Secured  {

  def globalsocket = withMaybeUserSocket[String, String] { implicit ctx => implicit req =>
    Right(ActorFlow.actorRef(WebSocketWorker.props))
  }


  def socket(userTimeline: Int) = withUserSocket[String, String] { implicit ctx => implicit req =>
    ActorFlow.actorRef( WebSocketWorker.speficicprops(Some(userTimeline), _) )
  }

}
