package controllers

import akka.actor.ActorRef
import akka.stream.scaladsl.Flow
import com.google.inject.Inject
import models.User
import play.api.mvc._
import services.{UserService, WebSocketWorker}

import scala.concurrent.Future
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import play.api.libs.streams.ActorFlow
import play.api.mvc.WebSocket.MessageFlowTransformer

case class Context(user: Option[User])

trait Secured {

  @Inject() var userservice: UserService = _

  def username(request: RequestHeader) = request.session.get(Security.username)
  def authString(request: RequestHeader) = request.session.get("userauth")

  def onUnauthorized(request: RequestHeader) = Results.Redirect(routes.AuthController.login)

  def withAuth(f: => String => Request[AnyContent] => Future[Result]) = {
    Security.Authenticated(authString, onUnauthorized) { userauth =>
      Action.async(request => f(userauth)(request))
    }
  }

  /**
    * This method shows how you could wrap the withAuth method to also fetch your user
    * You will need to implement UserDAO.findOneByUsername
    *//*
  def withUser(f: Context => Request[AnyContent] => Future[Result]) = withAuth { userauth => implicit request =>
    val maybeFutureUser = userservice.getUserByAuth(userauth)
    val response = maybeFutureUser.map {
      case Some(user) => f(Context(Some(user)))(request)
      case None => Future.successful(onUnauthorized(request))
    }

    response.flatMap(identity)
  }*/

  def withUser(f: Context => Request[AnyContent] => Future[Result]) = withMaybeUser { implicit context => implicit request =>

    context.user match {
      case Some(user) => f(context)(request)
      case None => Future.successful(onUnauthorized(request))
    }

  }
  def withMaybeUser(f: Context => Request[AnyContent] => Future[Result]) = Action.async{ implicit request =>
    val userauther = authString(request)

    val mapping: Future[Option[User]] = userauther match {
      case Some(str) => userservice.getUserByAuth(str)
      case None => Future.successful(None)
    }

    val result = mapping.map { maybeUser =>
      f(Context( maybeUser ))(request)
    }

    result.flatMap(identity)
  }

  def withMaybeUserSocket[I,O](f: Context => RequestHeader => Either[Result, Flow[I,O, _]])(implicit mf: MessageFlowTransformer[I,O]) = WebSocket.acceptOrResult[I, O] { implicit request =>
    val userauther = authString(request)

    val mapping: Future[Option[User]] = userauther match {
      case Some(str) => userservice.getUserByAuth(str)
      case None => Future.successful(None)
    }

    val result: Future[Either[Result,  Flow[I,O, _]]] = mapping.map { user =>
      f(Context(user))(request)
    }
    result
  }

  def withUserSocket[I,O](f: Context => RequestHeader => Flow[I,O, _])(implicit mf: MessageFlowTransformer[I,O])  = withMaybeUserSocket[I,O] { implicit x => implicit y =>
    x.user match {
      case None => Left(Results.Forbidden)
      case Some(us) => Right(f(x)(y))
    }
  }
}