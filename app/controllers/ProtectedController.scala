package controllers

import play.api.mvc.{Action, Controller}

/**
  * Created by domi on 12-Oct-16.
  */
class ProtectedController extends Controller {

  def save = Action {
    Ok("Ich sollte nur für eingeloggte sichtbar sein!");
  }

}
