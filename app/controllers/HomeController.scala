package controllers

import javax.inject._

import models.{Message, PrintMessage}
import play.api._
import play.api.mvc._

import scala.concurrent.Future
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import services.{MessageService, UserService}

/**
 * This controller creates an `Action` to handle HTTP requests to the
 * application's home page.
 */
@Singleton
class HomeController @Inject()(msgs: MessageService, uese: UserService) extends Controller with Secured {

  /**
   * Create an Action to render an HTML page with a welcome message.
   * The configuration in the `routes` file means that this method
   * will be called when the application receives a `GET` request with
   * a path of `/`.
   */
  val pageSize:Long = 10L
  def readtoots(offset:Long, max:Long): Future[List[PrintMessage]] ={
    val twoots = msgs.getGlobal(offset, Some(max))
    val result = twoots.map { list =>
      val converted = for {
        twoot <- list
      } yield convertMessage(twoot)
      val sequenced = Future.sequence(converted)
      sequenced.map { list =>
        list.flatten
      }
    }
    result.flatMap(identity)
  }
  def index(page: Long) = withMaybeUser { implicit ctx =>  implicit req =>
    val offset = page * this.pageSize
    val max = this.pageSize - 1
    val list = readtoots(offset,max)
    val amount = msgs.amountGlobal

    for {
      twoots <- list
      amt <- amount
    } yield Ok(views.html.index(twoots, page, pageSize, amt))
    //Future.successful()
  }
  def convertMessage(msg: Message): Future[Option[PrintMessage]] = msgs.convertMessage(msg)
}
