package controllers

import javax.inject.{Inject, Singleton}

import models.{BasicUser, LoginUser, User}
import play.api.data._
import play.api.data.Forms._
import play.api.i18n.{I18nSupport, MessagesApi}
import play.api.mvc.{Action, Controller}
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import services.{SessionService, UserService}

import scala.concurrent.Future

/**
  * Created by domi on 12-Oct-16.
  */
@Singleton
class AuthController @Inject()( users: UserService, sese: SessionService, val messagesApi: MessagesApi ) extends Controller with Secured with I18nSupport {

  val loginFormular :Form[models.LoginUser] = Form(
    mapping(
      "email" -> email,
      "password" -> nonEmptyText(minLength = 8, maxLength = 16)
    )(LoginUser.apply)(LoginUser.unapply)
  )

  val registerFormular = Form(
    mapping(
      "username" -> nonEmptyText(minLength = 5, maxLength = 16) ,
      "email" -> email,
      "password" -> nonEmptyText(minLength = 8, maxLength = 16)
    )(BasicUser.apply)(BasicUser.unapply)
  )

  def loginForm = withMaybeUser { implicit ctx => implicit req =>
    Future.successful(Ok(views.html.login(loginFormular)))
  }

  def login = withMaybeUser { implicit ctx => implicit request =>

    this.loginFormular.bindFromRequest.fold(
      formWithErrors => {
        Future.successful(Ok(views.html.login(formWithErrors)).flashing("errors" -> "Ungültige Logindaten"))
      },
      loginData => {
        val res = users.login(loginData.email, loginData.password  )
        val resa = res.map {
            case Some(usr) =>
              val authId = sese.addSession(request, usr)
              authId.map {
                case Some((key, value)) => Redirect(routes.HomeController.index(0)).withSession(request.session + (key, value))
                case None => Redirect(routes.AuthController.loginForm()).flashing("errors" -> "Login fehlgeschlagen!")
              }
            case None => Future.successful( Redirect(routes.AuthController.loginForm()).flashing("errors" -> "Ungültige Logindaten"))
        }
        resa.flatMap(identity)
      }
    )
  }

  def registerForm = withMaybeUser { implicit ctx => implicit req =>
    Future.successful(Ok(views.html.register(this.registerFormular)))
  }

  def register = withMaybeUser { implicit ctx => implicit request =>
    this.registerFormular.bindFromRequest.fold(
      formWithErrors => {
          Future.successful(Ok(views.html.register(formWithErrors)).flashing("errors" -> "Bitte überprüfen Sie ihre Eingaben!"))
      },
      userData => {
        /* Check if worked, than say "Check your mails" or "Login" or so */
        val successful = users.add(userData)

        successful.map {
          case true => Redirect(routes.AuthController.registerForm()).flashing("success" -> "Registration erfolgreich!")
          case false => Redirect(routes.AuthController.registerForm()).flashing("errors" -> "Userdaten bereits vergeben!")
        }
      }
    )
  }

  //http://stackoverflow.com/questions/21013345/helpers-in-play-and-checking-if-an-user-is-authenticated-in-a-view

  def logout = withUser { implicit user => implicit request => /// { implicit request =>
    val logout = sese.delSession(request)
    val result = logout.map {
      case true => Redirect(routes.HomeController.index(0)).withSession(request.session - "userauth" )
      case false => Ok("Logout nicht möglich - Sorry! Bitte melde dich an unseren Systemadministrator")
    }
    result
  }

}
