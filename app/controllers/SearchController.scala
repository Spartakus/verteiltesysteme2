package controllers

import javax.inject.{Inject, Singleton}

import models.User
import play.api.mvc.Controller
import services.{MessageService, UserService}
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import play.api.libs.json.{JsArray, Json}

import scala.util.parsing.json.JSONArray

/**
  * Created by domi on 11.12.2016.
  */
@Singleton
class SearchController @Inject()(uese: UserService) extends Controller with Secured {
  def search(searchStr: String) = withMaybeUser{ implicit ctx => implicit req =>
    val searchRes = uese.searchUsers(searchStr)
    searchRes.map { list =>
      Ok(views.html.searchresult(list))
    }
  }

  def jsonsearch(searchStr: String) = withMaybeUser{ implicit ctx => implicit req =>
    val searchRes = uese.searchUsers(searchStr)
    searchRes.map { list =>

      val otherList = list.map { user =>
        Json.obj( "id" -> user.id, "text" -> user.username )
      }

      val objects = new JsArray
      /*
      val otherlist = for {
        i <- 0 until list.size
      } yield Json.obj( "id" -> i, "text" -> list(i).username)*/

      val newArr = objects ++ JsArray(otherList)
      Ok(newArr)
    }
  }
}