package dao


import com.github.t3hnar.bcrypt._
import com.google.inject.ImplementedBy
import models._
import scredis.LexicalScoreLimit.{Exclusive, Inclusive}

import scala.concurrent.Future
import scala.collection.mutable.HashSet
import scala.concurrent.ExecutionContext.Implicits.global
import scredis._

import scala.util.{Failure, Success}


/*
  * Created by domi on 15.10.2016.
  */
@ImplementedBy(classOf[RedisUserManagement])
trait UserDAO {
  def addUser(user:BasicUser): Future[Boolean]
  def getById(id : UserId): Future[Option[User]]
  def delete(id : UserId): Future[Boolean]
  def listAll: Future[List[User]]
  def findByMailAndPw(email: String, pw: String) : Future[Option[User]]
  def getFollowing(user: User): Future[List[UserId]]
  def getFollowers(user: User): Future[List[UserId]]
  def addFollowing(target: User, following: User): Future[Boolean]
  def addFollowers(target: User, follower: User): Future[Boolean]
  def removeFollowing(target: User, following: User): Future[Boolean]
  def removeFollowers(target: User, follower: User): Future[Boolean]
  def login(email: String, pw: String) : Future[Option[User]]
  def findByMail(email: String) : Option[User]
  def getUserByAuth(authString: String): Future[Option[User]]
  def delAuthString(user: User): Future[Boolean]
  def addAuthString(user: User): Future[Option[String]]

  def searchUsersByPattern(pattern: String): Future[List[User]]

  def findByName(username: String): Future[Option[User]]
  def findByEmail(username: String): Future[Option[User]]
}

/*
  Redis Class. Redis Users having the following structure
  next_user_id        VALIE                           | INCR for next ID
  userids             SET                             | Contains ids
  usernames           SET                             | Contains usernames
  usermails           SET                             | Contains emails
  userattrib:ID       HASH(username, email, password) | Maps id to information
  useremail:EMAIL     VALUE                           | Maps Email to Id
  username:NAME       VALUE                           | Maps Name to Id
  userfollower:id     SET                             | Contains follwing user ids
  userfollowing:id    SET                             | Contains follower user ids
 */
class RedisUserManagement extends UserDAO {

  val redis = Redis()

  def addUser(user: BasicUser): Future[Boolean] = {
    val emailGiven = findByEmail(user.email)
    val nameGiven = findByName(user.username)

    val alreadyTaken = for {
      emailWeg <- emailGiven
      nameWeg <- nameGiven
    } yield emailWeg.isDefined || nameWeg.isDefined

    val output = alreadyTaken.map {
      case true => Future.successful(false)
      case false =>
        val nextId = redis.incr("next_user_id")
        val attributes = Map (
          "email" -> user.email,
          "username" -> user.username,
          "password" -> user.password.bcrypt
        )
        nextId.map{ i:UserId =>
          redis.sAdd("userids", i)

          redis.hmSet(s"userattrib:$i", attributes)
          redis.set(s"useremail:${user.email.toLowerCase()}", i)
          redis.set(s"username:${user.username.toLowerCase()}", i)

          redis.zAdd("usernames", Map(user.username.toLowerCase() -> 0  )) // Aus Set mache SortedSet !
          redis.sAdd("usermails", user.email.toLowerCase())
          true
        }
    }
    output.flatMap(identity)
  }

  def getById(id : UserId): Future[Option[User]] = {
    val userAttrib = redis.hmGet(s"userattrib:$id", "email", "username", "password")
    val out = userAttrib.map { listOfMaybes =>
      val maybeList = if (listOfMaybes.exists(_.isEmpty)) None else Some(listOfMaybes.map(_.get))
      maybeList.map { liste =>
        val email :: username :: password :: xs = liste
        val followers = this.getFollowers(id)
        val followings = this.getFollowings(id)
        val res = for {
          follows <- followers
          following <- followings
        } yield Some(User(id, username, email, password, follows, following))
        res
      }.getOrElse( Future.successful(None) )
    }.flatMap(identity)
    out
  }

  def delete(id : UserId): Future[Boolean] = Future(false)
  def listAll: Future[List[User]] = Future(List.empty)
  def findByMailAndPw(email: String, pw: String) : Future[Option[User]] = {
    val mail = email.toLowerCase()
    val userid = redis.get(s"useremail:$mail")

    val retB = userid.map {
        case Some(userId) =>
          val returnUser = this.getById(userId.toLong)
          returnUser.map {
            case Some(usr) => if ( pw.isBcrypted(usr.password) )  Some(usr) else None
            case _ => None
          }
        case None => Future.successful{ None }
    }

    retB.flatMap(identity)
  }

  def getFollowing(user: User): Future[List[UserId]] = this.getFollowings(user.id)
  def getFollowers(user: User): Future[List[UserId]] = this.getFollowers(user.id)

  def addFollowing(target: User, Following: User): Future[Boolean] = this.addFollowing(target.id, Following.id)
  def addFollowers(target: User, Follower: User): Future[Boolean] = this.addFollower(target.id, Follower.id)
  def removeFollowing(target: User, Following: User): Future[Boolean] = this.remFollowing(target.id, Following.id)
  def removeFollowers(target: User, Follower: User): Future[Boolean] = this.remFollower(target.id, Follower.id)

  def login(email: String, pw: String) : Future[Option[User]] = findByMailAndPw(email, pw)

  def findByMail(email: String) : Option[User] = None

  // Takes a user and generates an authentication string
  def addAuthString(user: User): Future[Option[String]] =  {
    val randomUUID = java.util.UUID.randomUUID.toString
    redis.hmSet(s"userattrib:${user.id}", Map("auth" -> randomUUID) )
    redis.set(s"userauth:$randomUUID", user.id ).map { x =>
        if (x) Some(randomUUID)
        else None
    }
  }

  def delAuthString(user: User): Future[Boolean] = {

    // Take User, read his authstr - then delete his auths everywhere
    val getUserAuth = redis.hmGet(s"userattrib:${user.id}", "auth")
    getUserAuth.map{
      _.head match {
        case None => Future.successful(false)
        case Some(authstr) =>

          val userFromAuth = this.getUserByAuth(authstr)

          val removeFromUserAuth = redis.del(s"userauth:$authstr").map{
            x: Long =>
              if (x > 0) true
              else false
          }

          val removeFromUser = userFromAuth.map{
            case None => Future.successful(true)
            case Some(usr) =>  redis.hDel(s"userattrib:${usr.id}", "auth").map(
              x =>
                if (x > 0) true
                else false
            )
          }.flatMap(identity)
          val result = for {
            rFUA <- removeFromUserAuth
            rFU <- removeFromUser
          } yield rFUA && rFU

          result
      }
    }.flatMap(identity)
  }

  def getUserByAuth(authString: String): Future[Option[User]] = {
    val out = redis.get(s"userauth:$authString").map {
      case None => Future.successful(None)
      case Some(userId) => this.getById(userId.toLong).map {
        futureOfMaybeUser => futureOfMaybeUser
      }
    }
    out.flatMap(identity)
  }


  def findByName(username: String): Future[Option[User]] = {
    val un = username.toLowerCase
    val search = redis.get(s"username:$un")
    search.map {
      case Some(id) => this.getById(id.toLong)
      case None => Future.successful(None)
    }.flatMap(identity)
  }

  def findByEmail(usermail: String): Future[Option[User]] = {
    val email = usermail.toLowerCase()
    val searchForMail = redis.sIsMember("usermails", email)
    val result = searchForMail.map {
      case true =>
        val userid = redis.get(s"useremail:$email")
        val res = userid.map {
          case Some(id) => this.getById(id.toLong)
          case None => Future.successful(None)
        }
        res.flatMap(identity)
      case false => Future.successful(None)
    }
    result.flatMap(identity)
  }


  def searchUsersByPattern(pattern: String): Future[List[User]] =  {

    val patterLower = pattern.toLowerCase()

    val lowerBound = Inclusive(patterLower)

    val upperStr = pattern.take(patterLower.length-1) + (patterLower.last + 1).toChar
    val upperBound = Exclusive(upperStr.toLowerCase())

    val redisSearch = redis.zRangeByLex("usernames", lowerBound, upperBound)

    val res = redisSearch.map { list =>
      val inner = list.map { element =>
        this.findByName(element)
      }

      val inner2 = Future.sequence(inner).map {_.toList.flatten}
      inner2
    }

    res.flatMap(identity)
  }

  private def remFollower(target: UserId, follower: UserId): Future[Boolean] = {
    val res = redis.zRem(s"userfollower:$target", follower)
    res.map { amount =>
      amount > 0
    }
  }

  private def remFollowing(target: UserId, following: UserId): Future[Boolean] = {
    val res = redis.zRem(s"userfollowing:$target", following)
    res.map { amount =>
      amount > 0
    }
  }

  private def addFollower(target: UserId, follower: UserId): Future[Boolean] = {
    redis.zAdd(s"userfollower:$target", follower, this.timestamp())
  }

  private def addFollowing(target: UserId, following: UserId): Future[Boolean] = {
    redis.zAdd(s"userfollowing:$target", following, this.timestamp())
  }


  private def getFollowers(userid: UserId): Future[List[UserId]] = {
    //userfollower:id
    val list = redis.zRevRange(s"userfollower:$userid")
    list.map { liste =>
      liste.map{ _.toLong }.toList
    }
  }

  private def getFollowings(userid: UserId): Future[List[UserId]] = {
    //userfollower:id
    val list = redis.zRevRange(s"userfollowing:$userid")
    list.map { liste =>
      liste.map{ _.toLong }.toList
    }
  }

  private def timestamp(): Long = System.currentTimeMillis / 1000
}