package dao

import akka.japi.Util
import com.google.inject.{ImplementedBy, Inject}
import models._
import scredis._
import scredis.ScoreLimit.{Inclusive, PlusInfinity}
import services.{PublishToQueue, UserService}

import scala.concurrent.Future
import scala.util.{Failure, Success}

/**
  * Created by domi on 19.10.2016.
  */
@ImplementedBy(classOf[RedisMessageManagement])
trait MessageDAO {
  def addMessage( msg: postMessage, user: User  ): Future[Boolean]
  def removeMessage( msg: Message ): Future[Boolean]

  def getById(msg: MessageId): Future[Option[Message]]
  def getTimelineOfUser( userId: UserId, offset: Long = 0, limit: Option[Long]): Future[List[Message]]
  def getGlobal( offset: Long = 0, limit: Option[Long]): Future[List[Message]]

  def getAmountGlobal: Future[Long]
  def getAmountOnTimeline( userId: UserId ): Future[Long]
  def getAmountByAuthor( userId: UserId ): Future[Long]

  def removeMessagesOfAuthorFromTimeline( userWithTimeline: UserId, userWithMsgToBeDeleted: UserId  ): Future[Boolean]
  def addMessagesOfAuthorToTimeline( userWithTimeline: UserId, userWithMsgToBeadded: UserId  ): Future[Boolean]

  def convertMessage(msg: Message): Future[Option[PrintMessage]]
}

/** Following Structures
  * next_message_id   | VALUE       | INCR for next ID
  * global_messages   | sorted SET  | containing all message ids
  * messages:msg_id   | HASH        | contain message detail (body, date)
  * user:ID:messages  | sorted SET  | containing messageids from given user
  * user:ID:timeline  | sorted SET  | containing all messageids for the user (FROM user and his followers)
*/
class RedisMessageManagement extends MessageDAO {
  //* Private Dependendys */
  val redis = Redis()
  @Inject() var userservice: UserService = _

  import redis.dispatcher
  /* Interface Implementation */
  def addMessage( msg: postMessage, user: User ): Future[Boolean] = {
    val time = this.timestamp()
    val nextId = redis.incr("next_message_id")
    val attributes = Map (
      "body" -> msg.body,
      "owner" -> user.id.toString,
      "date" ->  time.toString
    )
    val resultAdd = nextId.map { msgid:MessageId =>
      redis.hmSet(s"messages:$msgid", attributes)

       val addedMessage = for {
        aToUser <- addToUserMessage(msgid, time, user)
        aToTimeLines <- addToTimelines(msgid, time, user)
        addToGlobal <- addToGlobal(msgid, time)
      } yield aToUser && aToTimeLines && addToGlobal

      addedMessage.map {
        case true => Some(msgid)
        case false => None
      }
    }.flatMap(identity)

    def publishUpdate(msgid: MessageId) = this.publishUpdate(user.id, msgid).map{ x =>
      redis.publish(s"GlobalTimeline", s"$msgid").map { y =>
        true
      }
    }

    val result = resultAdd.map {
      case Some(id) => publishUpdate(id).flatMap(identity)
      case None => Future.successful(false)
    }

    result.flatMap(identity)
  }

  def publishUpdate(timelineId: UserId, msgid: MessageId) = redis.publish(s"Timeline ${timelineId.toString}", s"$msgid")


  def removeMessage( msg: Message ): Future[Boolean] = {
    val user = userservice.getById(msg.owner)

    val result = user.map {
      case Some(use) =>
        for {
          remFromUser <- remFromUserMessage(msg.id, use)
          remFromTLs  <- remFromTimeLines(msg.id, use)
          remFromGlobal <- remFromGlobal(msg.id)
        } yield remFromUser && remFromTLs && remFromGlobal
      case None =>
        Future.successful(false)
    }

    result.flatMap(identity)
  }

  def getById(msg: MessageId): Future[Option[Message]]   = {
    val msgAttrib = redis.hmGet(s"messages:$msg", "body", "owner", "date")
    msgAttrib.map { liste =>
      // Set List to None, if one Attribute is None
      val maybeList = if (liste.exists(_.isEmpty)) None else Some(liste.map(_.get))

      maybeList.map { list =>
        val body :: owner :: date :: xs = list
        Some( Message( msg, owner.toLong, body, date ) )
      }.getOrElse(None)
    }
  }

  def getTimelineOfUser( userId: UserId, offset: Long = 0, limit: Option[Long]): Future[List[Message]] = {
    val maximum = limit match {
      case Some(limitint) => offset + limitint
      case None => -1
    }
    val msgIds = redis.zRevRange(s"user:$userId:timeline", offset, maximum)
    val res = msgIds.map { msgIds =>
      val msgIdsAsList = msgIds.toList
      val inner = msgIdsAsList.map { msgId =>
        this.getById(msgId.toLong)
      }

      val futureOfList = Future.sequence(inner) // Turn List of Futures into Future of List
      futureOfList.map { list =>
        list.flatten
      }
    }

    res.flatMap(identity)
  }

  def getGlobal( offset: Long = 0, limit: Option[Long]): Future[List[Message]] = {
    val maximum = limit match {
      case Some(limitint) => offset + limitint
      case None => -1
    }
    val msgIds = redis.zRevRange("global_messages", offset, maximum)
    val res = msgIds.map { msgIds =>
      val msgIdsAsList = msgIds.toList
      val inner = msgIdsAsList.map { msgId =>
        this.getById(msgId.toLong)
      }

      val futureOfList = Future.sequence(inner) // Turn List of Futures into Future of List
      futureOfList.map { list =>
        list.flatten
      }
    }

    res.flatMap(identity)
  }

  def getAmountGlobal: Future[Long] = redis.zCount("global_messages", Inclusive(0), PlusInfinity)
  def getAmountOnTimeline( userId: UserId ): Future[Long] = redis.zCount(s"user:$userId:timeline", Inclusive(0), PlusInfinity)
  def getAmountByAuthor( userId: UserId ): Future[Long] = redis.zCount(s"user:$userId:messages", Inclusive(0), PlusInfinity)


  def removeMessagesOfAuthorFromTimeline( userWithTimeline: UserId, userWithMsgToBeDeleted: UserId  ): Future[Boolean] = {
    // SADLY Redis lacks the option to Substract Sorted Sets (Sets only!) So Use this ``Workaround´´...
    val result = redis.inTransaction { t =>
      t.zInterStoreWeighted("temporaryIntersection", Map(s"user:$userWithTimeline:timeline" -> 1, s"user:$userWithMsgToBeDeleted:messages" -> 0))
      t.zUnionStoreWeighted(s"user:$userWithTimeline:timeline", Map(s"user:$userWithTimeline:timeline" -> 1, "temporaryIntersection" -> -1))
      t.zRemRangeByScore(s"user:$userWithTimeline:timeline", Inclusive(0), Inclusive(0))
      t.del("temporaryIntersection")
    }
    result.map { results =>
      //val failed = results.filter( x => case Failure(x) => x)
      !results.contains(Failure)
    }
  }

  def addMessagesOfAuthorToTimeline( userWithTimeline: UserId, userWithMsgToBeadded: UserId  ): Future[Boolean] = {
    // this one is easy
    val amount = redis.zUnionStore(s"user:$userWithTimeline:timeline", Seq(s"user:$userWithTimeline:timeline", s"user:$userWithMsgToBeadded:messages"))
    amount.map { x =>
      if(x > 0) true
      else false
    }
  }


  def convertMessage(msg: Message): Future[Option[PrintMessage]] = {
    val user = userservice.getById(msg.owner)
    user.map{
      case Some(usr) => Some(PrintMessage(msg.id, usr, msg.body, msg.date))
      case None => None
    }
  }


  /* Private Functions */

  /**
    * Generates a timestamp
    * @return current timestamp
    */
  private def timestamp(): Long = System.currentTimeMillis / 1000

  // Adders

  /**
    * Adds Msg to users personal Message List
    * @param msgid Message Id
    * @param time Post Time (for score)
    * @param user The given User
    * @return future True or false
    */
  private def addToUserMessage( msgid: MessageId, time: Long, user: User ): Future[Boolean] = {
    redis.zAdd(s"user:${user.id}:messages", msgid, time)
  }

  /**
    * Adds Msg to timelines of authors followers
    * @param msgid msgid
    * @param time timestamp for score
    * @param user author (Having the followers)
    * @return future True or false
    */
  private def addToTimelines( msgid: MessageId, time: Long, user: User ): Future[Boolean] = {
    val targetUsers = user.followers.::(user.id)

    for {currentFollower <- targetUsers} this.publishUpdate(currentFollower, msgid)

    val futures = for {
      follower <- targetUsers
    } yield redis.zAdd(s"user:${follower}:timeline", msgid, time)

    val futureList = Future.sequence(futures)
    val mapListToSingle = futureList.map { list =>
      !list.contains(false) // Any false inside (Where add was not working?)
    }
    mapListToSingle
  }

  /**
    * Adds Msg to global timeline
    * @param msgid msgid
    * @param time timestamp for score
    * @return future True or false
    */
  private def addToGlobal( msgid: MessageId, time: Long ): Future[Boolean] = {
    redis.zAdd("global_messages", msgid, time)
  }

  // Removers
  /**
    * Remove msg from authors message list
    * @param msgId messageid
    * @param user user
    * @return future True or false
    */
  private def remFromUserMessage( msgId: MessageId, user: User ): Future[Boolean] = {
    redis.zRem(s"user:${user.id}:messages", msgId).map { amount =>
      if( amount > 0 ) true
      else false
    }
  }

  /**
    * Remove msg from timelines of authors followers
    * @param msgId messageid
    * @param user user
    * @return future True or false (If one failed -> false)
    */
  private def remFromTimeLines( msgId: MessageId, user: User  ): Future[Boolean] = {
    val futures = for (follower <- user.followers) yield redis.zRem(s"user:${follower}:timeline", msgId)
    val futureList = Future.sequence(futures)
    futureList.map { list =>
      val booleans = list.map { amount =>
        if( amount > 0 ) true
        else false
      }
      !booleans.contains(false)
    }

  }

  /**
    * Remove message from Global timeline
    * @param msgId messageid
    * @return Future true or false
    */
  private def remFromGlobal( msgId: MessageId ): Future[Boolean] = {
    redis.zRem("global_messages", msgId).map { amount =>
      if( amount > 0 ) true
      else false
    }
  }


}