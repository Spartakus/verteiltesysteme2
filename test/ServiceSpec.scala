import org.scalatestplus.play._
import services.{MessageService, UserService}

import scala.concurrent.Await
import scala.concurrent.duration._
/**
  * Created by domi on 19.10.2016.
  */
class ServiceSpec extends PlaySpec with OneAppPerTest{
/*
  // Services to test
  val us = new UserService( new LocalUserManagement )
  val ms = new MessageService( new LocalMessageManagement )

  // Adding some dummy messages
  val m2 = models.Message( 2, 300, 200, "Test2", "11.12.2016" )
  val m3 = models.Message( 3, 200, 300, "Test3", "12.12.2016" )
  ms.addMessage(m2); ms.addMessage(m3)

  // Adding some dummy users
  val u1 = models.User( 1, "hansi", "hansi@hansi.de", "Ads89ajköfj4Asdh§$%vsvE$%UF", List() )
  val u2 = models.User( 2, "klausi", "klausi@hansi.de", "FvsvE$%UIJRF", List(2, 3) )
  val u3 = models.User( 3, "peterli", "peterli@hansi.de", "Fs89ajköfj4Asd$%UR", List(1, 3) )
  val u4 = models.User( 4, "henri", "henri@hansi.de", "Fj4Asdh§$%vsvE$F", List(1, 2, 3) )
  us.add(u1); us.add(u2); us.add(u3); us.add(u4)

  "MessageService" should {
    "add messages" in {
      val m  = models.Message( 1001, 200, 300, "Test", "10.12.2016" )
      ms.addMessage(m)

      val m3 = ms.getById(1001)
      val m4 = Await.result(m3, 60.seconds)
      m4 mustEqual Some(m)
    }

    "remove messages" in {
      ms.removeMsg(m2)
      val msg = Await.result(ms.getById(m2.id), 120.seconds)
      msg mustEqual None
    }
  }

  "UserService" should {
    "add users" in {
      val uX = models.User( 1001, "hansi", "hansi@hansi.de", "Ads89ajköfj4Asdh§$%vsvE$%UF", List() )
      us.add(uX)

      val uM = us.getById(1001)
      val uw = Await.result(uM, 60.seconds)
      uw mustEqual Some(uX)
    }

    "remove messages" in {
      us.delete(u2.id)
      val user = Await.result(us.getById(u2.id), 120.seconds)
      user mustEqual None
    }
  }*/

}
