import javax.print.attribute.standard.PrinterMessageFromOperator

import com.google.inject.Inject
import models.{PrintMessage, postMessage}
import org.scalatestplus.play._
import play.api.test._
import play.api.test.Helpers._
import services.{MessageService, UserService}

import scala.concurrent.duration._
import scala.concurrent.{Await, Future}
import scala.concurrent.Await

/**
 * Add your spec here.
 * You can mock out a whole application including requests, plugins etc.
 * For more information, consult the wiki.
 */
class ApplicationSpec @Inject()(us: UserService)(ms: MessageService) extends PlaySpec with OneAppPerTest {

  "Routes" should {

    "send 404 on a bad request" in  {
      route(app, FakeRequest(GET, "/boum")).map(status(_)) mustBe Some(NOT_FOUND)
    }

  }

  "HomeController" should {

    "render the index page" in {
      val home = route(app, FakeRequest(GET, "/")).get

      status(home) mustBe OK
      contentType(home) mustBe Some("text/html")
      contentAsString(home) must include ("Your new application is ready.")
    }

  }

  "CountController" should {

    "return an increasing count" in {
      contentAsString(route(app, FakeRequest(GET, "/count")).get) mustBe "0"
      contentAsString(route(app, FakeRequest(GET, "/count")).get) mustBe "1"
      contentAsString(route(app, FakeRequest(GET, "/count")).get) mustBe "2"
    }

  }
/*
  val post: postMessage = models.postMessage("Ich bin die Nachricht")
  ms.addMessage(postMessage)

  "MessageService" should {

    "add messages" in {
      val m  = models.Message( 1001, 200, 300, "Test", "10.12.2016" )
      ms.addMessage(m)

      val m3 = ms.getById(1)
      val m4 = Await.result(m3, 60.seconds)
      m4.get mustEqual(m)
    }

    "remove messages" in {
      ms.removeMsg(m2)
      val msg = Await.result(ms.getById(m2.id), 120.seconds)
      msg.get mustEqual(None)
    }
  }*/

}
