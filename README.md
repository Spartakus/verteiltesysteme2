This is a solution to the module "Distributed Systems II"
=================================
This solution is based on Scala and Play Framework.
It uses Redis for persistence and ActiveMQ for Websocket message processing in
STOMP.

Controllers
===========
- AsyncController.scala:

A dummy Controller. Shows how to handle asynchronous request handling via Future

- AuthController.scala:

Handles sign up, login, logout, etc.

- CountController.scala:

Yet another Dummy controller. Shows dependency injection of a service.

- HomeController.scala:

The Start page showing the newest messages as global timeline

- ProtectedController.scala:

Another dummy controller showing how to secure controllers and its actions.
only usable by logged in users.

- SearchController.scala:

Handles the usersearch

- Secured.scala:

Defines the auth. traits/mixins for being used with auth-sensitive controllers
(withMaybeUser ==> Option[User], withUser => User)

- TimelineController.scala:

Displays a given timeline of a user (no pagination for now)

-  WebsocketController.scala:

Handles Websocket Requests. Instantiates a "WebSocketWorker" that is about to
listen to ActiveMQ and its respective STOMP format.

Components
==========

- Models:

Defines the data classes / the domain

- DAOs:

A Layer for data access from and to redis.
(For user and message data objects)

- Services:

A List of various services. Most of them speak to our DAOs. Some of them 
wrap ActiveMQ and do business logic aswell.

SessionService e.g. handles cookie data to restore a user session on request.
WebSocketWorker handles "redis" messages.

- Util:

Handles different HTML-Header when logged in / out.
Also handles pagination.


WebSocket and Eventmessaging:
==============================
When a message is posted, it gets send to a redis message channel.
the RedisToQueue Service reads those and put them to ActiveMQ by PublishToQueue
Service.

From here, an ActiveMQ takes over the message handling on clientside via STOMP
and stomp-websocket.

QueueLauncher Service hooks into Lifecycle of Play to boot an locale ActiveMQ
Instance if not already running.
It also downs ActiveMQ on downing the play instance.
(Ofc. only happens if done gracefully)

Filters
=======

Unused.